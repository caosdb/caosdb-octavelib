[requires]
caosdb/[>=0.2.1, include_prerelease=True]

[build_requires]
boost/1.78.0
gtest/1.11.0

[generators]
cmake
