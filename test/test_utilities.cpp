/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "caosdb/exceptions.h"
#include "maoxdb.hpp"
#include "mex.h"
#include <gtest/gtest.h>
#include <limits>
#include <string>
#include <vector>

namespace maoxdb {

///////////////////////////////////////////////////////////////////////////////
//                              Helper functions                             //
///////////////////////////////////////////////////////////////////////////////

/*
 * These functions test if the value can be converted to a mex scalar.
 */
void test_scalar_uint64(UINT64_T value) {
  mxArray *scalar = mxScalarUINT64(value);
  EXPECT_TRUE(mxIsUint64(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 1);
  EXPECT_EQ(mxGetScalarValue<UINT64_T>(scalar), value);
}

void test_scalar_int64(INT64_T value) {
  mxArray *scalar = mxScalarINT64(value);
  EXPECT_TRUE(mxIsInt64(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 1);
  EXPECT_EQ(mxGetScalarValue<INT64_T>(scalar), value);
}

void test_scalar_double(double value) {
  mxArray *scalar = mxScalarDOUBLE(value);
  EXPECT_TRUE(mxIsDouble(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 1);
  EXPECT_EQ(mxGetScalarValue<double>(scalar), value);
}

void test_scalar_logical(bool value) {
  mxArray *scalar = mxScalarLOGICAL(value);
  EXPECT_TRUE(mxIsLogical(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 1);
  EXPECT_EQ(mxGetScalarValue<bool>(scalar), value);
}

void test_scalar_empty() {
  mxArray *scalar = mxEmptyDOUBLE();
  EXPECT_TRUE(mxIsDouble(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);

  scalar = mxEmptyUINT64();
  EXPECT_TRUE(mxIsUint64(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);

  scalar = mxEmptyINT64();
  EXPECT_TRUE(mxIsInt64(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);

  scalar = mxEmptyLOGICAL();
  EXPECT_TRUE(mxIsLogical(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);

  scalar = mxEmptyCHAR();
  EXPECT_TRUE(mxIsChar(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);

  scalar = mxEmptySTRING();
  EXPECT_TRUE(mxIsChar(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);
  EXPECT_EQ(mxGetNumberOfDimensions(scalar), 2);

  scalar = mxEmptySparse();
  EXPECT_TRUE(mxIsSparse(scalar));
  EXPECT_TRUE(mxIsEmpty(scalar));
  EXPECT_EQ(mxGetNumberOfElements(scalar), 0);
}

/**
 * Generate a vector with useful values for testing.
 */
template <typename T> auto values() -> std::vector<T> {
  std::vector<T> values = {
    0,
    1,
    std::numeric_limits<T>::max(),
    std::numeric_limits<T>::min(),
    std::numeric_limits<T>::lowest(),
    std::numeric_limits<T>::epsilon() // 0 for integers, but who cares?
  };
  return values;
}

///////////////////////////////////////////////////////////////////////////////
//                                Actual tests                               //
///////////////////////////////////////////////////////////////////////////////

/**
 * Test if construction of scalar mex arrays works
 */
TEST(utilities, scalar_arrays) {
  auto values_uint64 = values<UINT64_T>();
  for (auto value : values_uint64) {
    test_scalar_uint64(value);
  }

  auto values_int64 = values<INT64_T>();
  for (auto value : values_int64) {
    test_scalar_int64(value);
  }

  auto values_double = values<double>();
  for (auto value : values_double) {
    test_scalar_double(value);
  }

  std::vector<bool> values_logical = {true, false};
  for (auto value : values_logical) {
    test_scalar_logical(value);
  }
}

TEST(utilities, empty_array) { test_scalar_empty(); }

TEST(utilities, empty_cellstring) {
  auto empty = mxEmptyCell();
  auto empty_strings = mxCellToStrings(empty);
  EXPECT_TRUE(empty_strings.empty());

  empty = mxEmptyStruct();
  EXPECT_EQ(mxGetNumberOfElements(empty), 0);
}

TEST(utilities, duplicateArray) {
  // Sparse empty array
  auto sparse_array = mxEmptySparse();
  auto sparse_dup = mxDuplicateArrayAlsoSparse(sparse_array);
  EXPECT_TRUE(mxIsSparse(sparse_dup));
  EXPECT_EQ(mxGetNumberOfElements(sparse_array), mxGetNumberOfElements(sparse_dup));

  // Simple double array
  auto number_array = mxCreateDoubleMatrix(2, 2, mxREAL);
  double number_data[4] = {0.0, 1.0, 0.0 / 0.0, 3.14};
  mxSetData(number_array, number_data);
  auto number_dup = mxDuplicateArrayAlsoSparse(number_array);
  auto number_data_dup = static_cast<double *>(mxGetData(number_dup));
  EXPECT_EQ(number_data_dup[3], 3.14);
  EXPECT_FALSE(number_data_dup[2] == number_data_dup[2]);
  EXPECT_EQ(mxGetNumberOfElements(number_array), mxGetNumberOfElements(number_dup));

  // Struct array
  auto fields = std::vector<const char *>{"role", "value", "errors"};
  auto errors_cell = mxCreateCellMatrix(1, 2);
  mxSetCell(errors_cell, 0, mxScalarINT64(23));
  mxSetCell(errors_cell, 1, mxCreateString("twenty three"));
  auto struct_array = mxCreateStructFromStrings(fields);
  mxSetField(struct_array, 0, "role", mxCreateString("MY_ROLE"));
  mxSetField(struct_array, 0, "value", mxScalarDOUBLE(23.42));
  mxSetField(struct_array, 0, "errors", errors_cell);
  auto struct_dup = mxDuplicateArrayAlsoSparse(struct_array);
  auto struct_data_role_dup = mxGetStdString(mxGetField(struct_dup, 0, "role"));
  auto struct_data_value_dup = mxGetScalarValue<double>(mxGetField(struct_dup, 0, "value"));
  auto struct_data_errors_dup = mxGetField(struct_dup, 0, "errors");
  EXPECT_TRUE(struct_data_role_dup == "MY_ROLE");
  EXPECT_EQ(struct_data_value_dup, 23.42);
  EXPECT_EQ(mxGetScalarValue<int64_T>(mxGetCell(struct_data_errors_dup, 0)), 23);
  EXPECT_EQ(mxGetStdString(mxGetCell(struct_data_errors_dup, 1)), "twenty three");
}

/**
 * Test exception handling
 */

TEST(utilities, exception_handling) {
  caosdb::exceptions::AuthenticationError exc("Authentication failed.");
  auto strings = exceptionToMessage(exc);
  EXPECT_EQ(strings.first, std::string("16"));
  EXPECT_EQ(strings.second, std::string("The attempt to execute this transaction has not been "
                                        "executed at all because the authentication did not "
                                        "succeed.\nAuthentication failed."));
}

} // namespace maoxdb
