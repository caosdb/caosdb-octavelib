% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

function test_suite = test_unittest()
  try % assignment of 'localfunctions' is necessary in Matlab >= 2016
    test_functions = localfunctions();
  catch % no problem; early Matlab versions can use initTestSuite fine
  end
  initTestSuite;
end

% Only tests which don't need a connection to a server.
function test_local()
  % default connection
  c1 = Caosdb();
  assertEqual(c1.connection, "");
  % class with explicit connection
  c2 = Caosdb("local-caosdb-admin");
  assertEqual(c2.connection, "local-caosdb-admin");

  % Only get the local versions.
  version = caosdb_exec("--version");
  assertEqual(version(1:17), "v0.1 (libcaosdb v");
end

% Test if conversion between Entity objects and mxArray structures works.
function test_entity_conversion()
  % set plain Octave properties
  p1 = Property();
  p1.id = "P1_id";
  p1.name = "Prop_1";
  p1.description = "";
  p1.importance = "somewhat";
  p1.set_datatype("DOUBLE");
  p1.unit = "Mt";
  p1.value = [];

  p2 = Property();
  p2.id = "P2_id";
  p2.name = "Prop_2";
  p2.description = "";
  p2.importance = "very";
  p2.set_datatype("BOOLEAN");
  p2.value = 1;

  par = Parent();
  par.id = "-1";
  par.name = "Parent_1";
  par.description = "self-inheritance";

  e = Entity();
  e.role = "Record";
  e.id = "-1";
  e.name = "Test Record";

  % merge objects
  e.set_properties({p1, p2});
  e.set_parents({par});

  % convert forwards and backwards
  e_struct = e.to_struct();
  e_clone = Entity(e_struct);

  % Make sure that the conversion was lossless
  assertEqual(e_clone.id, e.id);
  assertEqual(numel(e.get_parents()), 1);
  assertEqual(e.get_parents_as_struct().Parent_1.description, par.description);
  assertEqual(numel(e.get_properties()), 2);
  assertEqual(e.get_properties_as_struct().Prop_1.value, p1.value);
  assertEqual(e.get_properties_as_struct().Prop_2.value, p2.value);
  assertTrue(isequal(struct(e), struct(e_clone)));
end
