# Changelog #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2023-02-17 ##

### Changed ###

- Updated necessary Conan version to 1.54.

## [0.2.0] - 2022-09-26 ##

### Added ###

- Jupyter notebook with examples was added in `doc/notebooks/`.

### Changed ###

- Depends on libcaosdb >= 0.2, boost-1.78.  Some other dependencies were updated as well.

## [0.1.2] - 2022-02-21 ##

### Changed ###

Documentation improved.

## [0.1.1] - 2022-02-04 ##

### Added ###

- Friendlier error messages when requirements at install time are not met.

## [0.1.0] - 2022-02-01 ##

### Added ###

- Full functionality of libcaosdb mapped to Octave / Matlab.
- Basic documentation (check out the integration tests for more examples).

### Changed ###

- Accepts any libcaosdb >= 0.1 now.

## [0.0.1] - 2021-08-12 ##

### Added ###

- Added Entity mapping, error handling, Entity retrieval and query execution.
