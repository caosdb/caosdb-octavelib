@c This file is a part of the CaosDB Project.
@c
@c Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
@c Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
@c
@c This program is free software: you can redistribute it and/or modify
@c it under the terms of the GNU Affero General Public License as
@c published by the Free Software Foundation, either version 3 of the
@c License, or (at your option) any later version.
@c
@c This program is distributed in the hope that it will be useful,
@c but WITHOUT ANY WARRANTY; without even the implied warranty of
@c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@c GNU Affero General Public License for more details.
@c
@c You should have received a copy of the GNU Affero General Public License
@c along with this program. If not, see <https://www.gnu.org/licenses/>.

@node Entity
@comment  node-name,  next,  previous,  up
@chapter The Entity class

@c %%%%%%%%%%%%%%% Entity %%%%%%%%%%%%%%%

@deftp Class Entity

This class represents Entities (Records, RecordTypes, Properties) in the CaosDB server, much like in
libcaosdb.  Detailed information can be found @url{https://docs.indiscale.com/caosdb-cpplib, there}.

@end deftp

@c %%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%
@deftypeivar Entity string role
One of ``@code{RECORD}'', ``@code{RECORD_TYPE}'',  ``@code{PROPERTY}'',  ``@code{FILE}''.
@end deftypeivar

@deftypeivar Entity string id
See libcaosdb.
@end deftypeivar
@deftypeivar Entity string versionId
@end deftypeivar
@deftypeivar Entity string name
@end deftypeivar
@deftypeivar Entity string description
@end deftypeivar
@deftypeivar Entity string unit
@end deftypeivar
@deftypeivar Entity array value
The exact type of the @var{value} property should match the datatype of the Entity object.
@end deftypeivar
@deftypeivar Entity array filepath
Only for ``@code{FILE}'' entities: the file path on the server.
@end deftypeivar
@deftypeivar Entity array localpath
Only for ``@code{FILE}'' entities: the local path to the file (for upload).
@end deftypeivar


@c %%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%

@defmethod Entity Entity ([struct @var{data}])

The constructor takes an optional @var{data} argument which may provide initial content.  The
typical user will just use the empty constructor @code{Entity()} and add content later.  (Details
about requirements for a valid @var{data} struct array can be found in the
@url{https://gitlab.indiscale.com/caosdb/src/caosdb-octavelib/-/blob/main/src/lib/maoxdb.hpp, source
code}.)
@end defmethod

@defmethod Entity get_datatype ()
@end defmethod
@defmethod Entity get_parents ()
@end defmethod
@defmethod Entity get_properties ()
@end defmethod
@defmethod Entity get_errors ()
@strong{Returns}
@param{, cell, A cell array of @ref{Message} objects@comma{} this Entity's errors.}
@end defmethod
@defmethod Entity get_warnings ()
@strong{Returns}
@param{, cell, A cell array of @ref{Message} objects@comma{} this Entity's warnings.}
@end defmethod
@defmethod Entity get_infos ()
@strong{Returns}
@param{, cell, A cell array of @ref{Message} objects@comma{} this Entity's info messages.}
@end defmethod

@defmethod Entity set_datatype (string @var{dtype_name}, [logical @var{is_reference} = false, logical @var{is_list} = false])
Set the datatype of this Entity in a consistent manner.

@strong{Parameters}

@param{dtype_name, string, Either the atomic datatype (for scalar simple values) or the ID of the referenced entity (in the case of reference values).}
@param{is_reference, logical, True if the value is a reference@comma{} else false (the default).}
@param{is_list, logical, True if the value is list types@comma{} false if it is a scalar (the default).}
@end defmethod

@defmethod Entity set_parents (cell @var{parents})
@strong{Parameters}
@param{parents, cell, A cell array with the parents (objects of class @var{Parent}).}
@end defmethod
@defmethod Entity set_properties (cell @var{properties})
@strong{Parameters}
@param{properties, cell, A cell array with the properties (objects of class @var{Property}).}
@end defmethod

@defmethod Entity has_errors ()
@strong{Returns}
@param{, logical, True iff this Entity has stored any error messages).}
@end defmethod
@defmethod Entity has_warnings ()
@strong{Returns}
@param{, logical, True iff this Entity has stored any warning messages).}
@end defmethod
@defmethod Entity has_infos ()
@strong{Returns}
@param{, logical, True iff this Entity has stored any info messages).}
@end defmethod

@defmethod Entity to_struct ([bool @var{warn}=true])
@strong{Note:} This method is mostly interesting for internal use of the library, end users probably
will never need it.

Convert to a struct which has all the fields that may be needed for interaction with the backend C++
functions.

If the @var{datatype} indicates a list value, the @var{value} is interpreted as such. It is an error
if the @var{value} is list-like (cell string or more than one numeric element) while the
@var{datatype} indicates a scalar value.
@end defmethod
