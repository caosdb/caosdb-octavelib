/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "caosdb/connection.h"         // for Connection, ConnectionManager
#include "caosdb/constants.h"          // for LIBCAOSDB_VERSION_MAJOR, LIBCAOSDB_VE...
#include "caosdb/exceptions.h"         // for all error handling
#include "caosdb/info.h"               // for VersionInfo
#include "caosdb/logging.h"            // for CAOSDB_LOG_TRACE
#include "caosdb/transaction_status.h" // for TransactionStatus
#include "maoxdb.hpp"                  // caosDB utils for mex files
#include "mex.h"                       // for mxArray, mexFunction
#include <algorithm>                   // for strcmp
#include <cstring>                     // for strcmp
#include <memory>                      // for unique_ptr, __shared_ptr_access, shar...
#include <string>                      // for allocator, char_traits, operator+

using caosdb::connection::Connection;
using caosdb::connection::ConnectionManager;
using caosdb::entity::Entity;
using caosdb::transaction::StatusCode;
using std::string;

const auto logger_name = "maox_transaction";
/**
 * @brief      Execute a rich transaction.
 *
 * @details    This function enables users to stage multiple actions and execute all of them in a
 * single transaction.
 *
 * @note       The code currently is a mix of all arguments required, and only some required (it may
 *             segfault with debugging logging if there are arguments missing).  It is
 *             definitely safer to have all arguments, but it should be pretty simple to remove this
 *             requirement if need be.
 *
 * @param connection_name A string with the connection name. May be omitted or
 *                        an empty string.
 *
 * @param retrieve_ids A cell string array of IDs to retrieve.
 *
 * @param queries A cell array of query strings (for retrieval).  More than one query is not
 *                supported at the moment though.
 *
 * @param inserts A struct array of Entities to insert.
 *
 * @param updates A struct array of Entities to update.  The behaviour is undefined if there are
 *                several Entities with the same ID.  The versionId of all Entities must be the
 *                same as the remote HEAD.
 *
 * @param deletes A cell array of IDs to delete.
 *
 * @param file_ids A cell array of IDs of files to download.
 *
 * @param file_download_paths A cell array of paths where the files shall be downloaded.  Must be at
 *                            least as large as the file_ids celll array.
 *
 * @return resultSet    A struct with the entities.
 *
 * @return countResult  The result of a COUNT query, is -1 if zero or more than one COUNT queries
 *                      were given.
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  string conn_name;
  std::vector<string> retrieves;
  std::vector<string> queries;
  std::vector<Entity> inserts;
  std::vector<Entity> updates;
  std::vector<string> deletes;
  std::vector<string> file_ids;
  std::vector<string> file_download_paths;
  // Argument handling
  auto helpText = string("maox_transaction(connection_name, retrieve_ids, queries, inserts, ") +
                  "updates, deletes, file_ids, file_paths)";
  try {
    CAOSDB_LOG_DEBUG(logger_name) << "maox_transaction(): " << nlhs << " <- " << nrhs << "("
                                  << (nrhs == 8) << ")"; // NOLINT
    CAOSDB_LOG_DEBUG(logger_name) << " ( " << mxGetNumberOfElements(prhs[0]) << ", "
                                  << mxGetNumberOfElements(prhs[1]) << ", "
                                  << mxGetNumberOfElements(prhs[2]) << ", "
                                  << mxGetNumberOfElements(prhs[3]) << ", "
                                  << mxGetNumberOfElements(prhs[4]) << ", "
                                  << mxGetNumberOfElements(prhs[5]) << ", "              // NOLINT
                                  << mxGetNumberOfElements(prhs[6]) << ", "              // NOLINT
                                  << mxGetNumberOfElements(prhs[7]) << ")" << std::endl; // NOLINT
    if (nrhs < 2) {
      mexErrMsgIdAndTxt("maox:InsufficientArguments",
                        (string("Need at least 2 arguments: ") + helpText).c_str());
    }
    if (nrhs > 8) { // NOLINT
      mexErrMsgIdAndTxt("maox:TooManyArguments",
                        (string("Can handle at most 8 arguments: ") + helpText).c_str());
    }
    if (nlhs < 2) {
      mexErrMsgIdAndTxt("maox:InsufficientReturnArguments",
                        "Need 2 return arguments: resultSet and queryCount.");
    }
    if (nlhs > 2) {
      mexErrMsgIdAndTxt("maox:TooManyReturnArguments",
                        "Need 2 return arguments: resultSet and queryCount.");
    }
    if (!mxIsChar(prhs[0])) {
      mexErrMsgIdAndTxt("maox:InvalidArgument", "The connection name must be a string.");
    }
    conn_name = maoxdb::mxGetStdString(prhs[0]);
    retrieves = maoxdb::mxCellToStrings(prhs[1]);
    if (nrhs > 2) {
      queries = maoxdb::mxCellToStrings(prhs[2]);
    }
    if (nrhs > 3) {
      inserts = maoxdb::entitiesFromMx(prhs[3], false, conn_name);
    }
    if (nrhs > 4) {
      updates = maoxdb::entitiesFromMx(prhs[4], true, conn_name);
    }
    if (nrhs > 5) {                               // NOLINT
      deletes = maoxdb::mxCellToStrings(prhs[5]); // NOLINT
    }
    if (nrhs == 7) { // NOLINT
      mexErrMsgIdAndTxt("maox:InvalidArgument", "File IDs need file download path arguments");
    }
    if (nrhs > 7) {                                           // NOLINT
      file_ids = maoxdb::mxCellToStrings(prhs[6]);            // NOLINT
      file_download_paths = maoxdb::mxCellToStrings(prhs[7]); // NOLINT
      if (file_ids.size() != file_download_paths.size()) {
        mexErrMsgIdAndTxt("maox:InvalidArgument",
                          "File IDs and file download paths must have the same number of elements");
      }
    }
  } catch (const std::exception &exc) {
    mexErrMsgIdAndTxt("maox:ArgumentHandling",
                      (string("Error while handling the arguments: ") + exc.what()).c_str());
  }

  // Execute the query
  std::shared_ptr<Connection> connection = nullptr;
  if (conn_name.empty()) {
    connection = ConnectionManager::GetDefaultConnection();
  } else {
    connection = ConnectionManager::GetConnection(conn_name);
  }
  auto transaction = connection->CreateTransaction();
  // Fill transaction with content
  std::for_each(retrieves.begin(), retrieves.end(),
                [&](const string &id) { transaction->RetrieveById(id); });
  // CAOSDB_LOG_DEBUG(logger_name) << "# queries: " << queries.size();
  std::for_each(queries.begin(), queries.end(), [&](const string &query) {
    CAOSDB_LOG_TRACE(logger_name) << "Adding query: " << query;
    transaction->Query(query);
  });
  std::for_each(inserts.begin(), inserts.end(),
                [&](Entity &ent) { transaction->InsertEntity(&ent); });
  std::for_each(updates.begin(), updates.end(),
                [&](Entity &ent) { transaction->UpdateEntity(&ent); });
  std::for_each(deletes.begin(), deletes.end(),
                [&](const string &id) { transaction->DeleteById(id); });

#pragma unroll 5
  for (size_t i = 0; i < file_ids.size(); ++i) {
    transaction->RetrieveAndDownloadFileById(file_ids[i], file_download_paths[i]);
  }
  // Execute transaction, if there is anything queued.
  if (transaction->GetStatus().GetCode() == StatusCode::READY ||
      transaction->GetStatus().GetCode() == StatusCode::GO_ON) {
    const auto exec_stat = transaction->ExecuteAsynchronously(); // NOLINT
    if (exec_stat != caosdb::StatusCode::EXECUTING) {
      mexErrMsgIdAndTxt("maox:Execution", (string("Executing the transaction failed (") +
                                           std::to_string(static_cast<int>(exec_stat)) +
                                           "): " + caosdb::get_status_description(exec_stat))
                                            .c_str());
    }
  }
  const auto t_stat = transaction->WaitForIt();
  maoxdb::throwOctExceptionIfError(transaction.get());
  // Status must be OK, INITIAL or GENERIC_TRANSACTION_ERROR now.
  CAOSDB_LOG_ERROR(logger_name) << "CALL GetResultSet 1";
  const auto &results = transaction->GetResultSet();
  CAOSDB_LOG_TRACE(logger_name) << "ResultSet contents: ";
  CAOSDB_LOG_TRACE(logger_name) << "size: " << results.size();

#pragma unroll 5
  for (const auto &result : results) {
    CAOSDB_LOG_TRACE(logger_name) << "errors = " << result.HasErrors();
    CAOSDB_LOG_TRACE(logger_name) << result.ToString();
  }
  auto *mxResults = maoxdb::mxFromResultSet(results);
  CAOSDB_LOG_TRACE(logger_name) << "results converted.";
  auto *mxCountResults = maoxdb::mxScalarINT64(transaction->GetCountResult());
  CAOSDB_LOG_TRACE(logger_name) << "counts converted.";

  plhs[0] = maoxdb::mxDuplicateArrayAlsoSparse(mxResults);
  plhs[1] = mxCountResults;
  CAOSDB_LOG_TRACE(logger_name) << "\n---";
  caosdb::utility::reset_arena();
}
