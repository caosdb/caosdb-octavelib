# Dependencies for caosdb-octavelib #

* caosdb-cpplib >= 0.2
* octave >= 4.4
* liboctave-dev (tested with 6.2.0)

## Useful tools ##

* Linter: [`miss_hit`](https://github.com/florianschanda/miss_hit), install with  `pip3 install --user miss_hit`
* Documentation generation: [`generate_html`](https://gnu-octave.github.io/packages/generate_html), install with  
  `pkg install "https://downloads.sourceforge.net/project/octave/Octave%20Forge%20Packages/Individual%20Package%20Releases/generate_html-0.3.2.tar.gz"`
