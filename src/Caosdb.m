% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%% Developer's note: Documentation may start to work with Octave 6, earlier versions cannot handle
% the help strings in classdef files.

% -*- texinfo -*-
% @deftypefn {Class} {} Caosdb ()
% This is the main class of the CaosDB client for Octave.
%
% Print usage help and return:
% @example
% caosdb --help;
% @end example
%
% Print the version and return:
% @example
% caosdb --version;
% @end example
%
% Test the default connection, print the server's version and return.
% @example
% caosdb --test-connection;
% @end example
%
% @end deftypefn

classdef Caosdb < handle
  properties
    connection = ""             % Will use the default connection
  end

  methods

    function obj = Caosdb(connection)
      if nargin == 0
        connection = "";
      end
      obj.connection = connection;
    end

    %%
    %% info
    %%
    function res = info(obj)
      try
        info_result = maox_info(obj.connection);
        res = info_result;
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Retrieve entities by IDs
    %
    % entities = Caosdb.retrieve_by_id("my_id", {"more", "ids"});
    %
    % Parameters
    % ----------
    %
    % ids: string or cell array of strings
    %  The ID(s) of the entity (entities) to be retrieved.
    %
    % Returns
    % -------
    % entities : cell array
    %  The retrieved entities.
    function entities = retrieve_by_id(obj, ids, varargin)
      % Ensure that IDS is a string cell array
      if ischar(ids)
        ids = {ids};
      else
        assert(iscellstr(ids), ...
               "maox:InvalidArgument", ...
               "IDS must be a string or string cell array, was:\n%s", ...
               disp(ids));
      end
      % Make one big cell array of potential strings
      for argin = varargin
        if ischar(argin)
          ids(end + 1) = argin;
        else
          assert(iscellstr(argin), ...
                 "maox:InvalidArgument", ...
                 "arguments must be a string (cell array), but this argument was found:\n%s", ...
                 disp(argin));
          ids(end + 1:end + numel(argin)) = argin(:);
        end
      end

      try
        [entities, count_results] = maox_run_transaction(obj.connection, ids);
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Execute a query
    %
    % entities = Caosdb.query("FIND Record Foo WITH bar=baz");
    % entities = Caosdb.query("COUNT Record Foo WITH bar=baz");
    %
    % Parameters
    % ----------
    %
    % query_str: string
    %  The query to be executed.
    %
    % Returns
    % -------
    % entities : cell array
    %  The retrieved entities.  If the query was a COUNT query, the result is an int64 instead.
    function entities = query(obj, query_str)
      % Ensure that QUERY is a string.
      assert(ischar(query_str), "maox:InvalidArgument", "QUERY must be a string, was:\n%s", ...
             disp(query_str));
      assert(nargin == 2, "maox:InvalidArgument", "This method accepts exactly 1 argument.");
      % disp("query:");
      % disp(query_str);
      try
        [entities, count_results] = maox_run_transaction(obj.connection, {}, {query_str});
        if count_results >= 0
          entities = count_results;
        end
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Insert an entity.
    %
    % inserted = Caosdb.insert(entities);
    %
    % Parameters
    % ----------
    %
    % entities: cell array
    %  A cell array with the entities to be inserted.
    %
    % Returns
    % -------
    % inserted : cell array
    %  The resulting inserted entities.
    function inserted = insert(obj, entities)
      % Ensure that entities is a cell array.
      assert(nargin == 2, "maox:InvalidArgument", "This method accepts exactly 1 argument.");
      assert(iscell(entities), "maox:InvalidArgument", ...
             "ENTITIES must be a cell array, was:\n%s", disp(entities));
      assert(numel(entities) <= 1, "maox:NotImplementedError", ...
             "Multi-insert has not been implemented yet.");
      % Convert the entities
      mx_entities = struct("", {});
      for entity = entities
        mx_entities(end + 1) = entity{1}.to_struct();
      end
      assert(numel(mx_entities) == numel(entities));

      % Create an run transaction
      try
        [inserted, count_results] = maox_run_transaction(obj.connection, {}, {}, mx_entities);
        assert(count_results == -1, "maox:RuntimeError", "There should be no count results.");
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Update an entity
    %
    % updated = Caosdb.update(entities);
    %
    % Parameters
    % ----------
    %
    % entities: cell array
    %  A cell array with the entities to be updated.  All entities must have a valid ID.
    %
    % Returns
    % -------
    % updated : cell array
    %  The resulting updated entities.
    function updated = update(obj, entities)
      % Ensure that entities is a cell array.
      assert(nargin == 2, "maox:InvalidArgument", "This method accepts exactly 1 argument.");
      assert(iscell(entities), "maox:InvalidArgument", ...
             "ENTITIES must be a cell array, was:\n%s", disp(entities));
      assert(numel(entities) <= 1, "maox:NotImplementedError", ...
             "Multi-update has not been implemented yet.");
      % Convert the entities
      mx_entities = struct("", {});
      % disp("UPDATE")
      for entity = entities
        mx_entities(end + 1) = entity{1}.to_struct();
        % disp(mx_entities(end))
      end
      assert(numel(mx_entities) == numel(entities));

      % Create an run transaction
      try
        [updated, count_results] = maox_run_transaction(obj.connection, {}, {}, {}, mx_entities);
        % disp(updated);
        assert(count_results == -1, "maox:RuntimeError", "There should be no count results.");
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Delete entities by IDs.
    %
    % entities = Caosdb.delete_by_id("my_id", {"more", "ids"});
    %
    % The usage is equivalent to retrieve_by_id(...).
    %
    % Note: Caosdb.delete will call the destructor of the Caosdb object.
    %
    % Parameters
    % ----------
    %
    % ids: string or cell array of strings
    %  The ID(s) of the entity (entities) to be retrieved.
    %
    % Returns
    % -------
    % entities : cell array
    %  The deleted entities (only IDs, possibly with messages).
    function entities = delete_by_id(obj, ids, varargin)
      % Ensure that IDS is a string cell array
      assert(nargin >= 2, "maox:InvalidArgument", "delete(...) needs at least one argument.");
      if ischar(ids)
        ids = {ids};
      else
        assert(iscellstr(ids), ...
               "maox:InvalidArgument", ...
               "IDS must be a string or string cell array, was:\n%s", ...
               disp(ids));
      end
      % Make one big cell array of potential strings
      if nargin == 2
        varargin = {};
      end
      for argin = varargin
        argin = argin{1};
        if ischar(argin)
          ids(end + 1) = argin;
        else
          assert(iscellstr(argin), ...
                 "maox:InvalidArgument", ...
                 "arguments must be a string (cell array), but this argument was found:\n%s", ...
                 disp(argin));
          ids(end + 1:end + numel(argin)) = argin(:);
        end
      end

      try
        % disp("IDs to delete:")
        % disp(ids);
        [entities, count_results] = maox_run_transaction(obj.connection, {}, {}, ...
                                                         struct("", {}), struct("", {}), ...
                                                         ids);
        assert(count_results == -1, "maox:RuntimeError", "There should be no count results.");
      catch
        rethrow(lasterror());
      end
    end

    %%
    % Download a file by ID
    %
    % entity = Caosdb.download_file_by_single_id("my_id", "/save/file/here.dat");
    %
    % Inserting files is done by creating a "FILE" Entity and inserting it.
    %
    % Parameters
    % ----------
    %
    % id: string
    %  The ID of the file to be downloaded.
    %
    % local_path: string
    %  The location where the file shall be saved.
    %
    % Returns
    % -------
    % entity : cell array
    %  1x1 cell array with the retrieved file entity.
    function entity = download_file_by_single_id(obj, id, local_path)
      narginchk(3, 3);
      assert(ischar(id), "maox:InvalidArgument", "Need a single ID as first argument.");
      assert(ischar(local_path), "maox:InvalidArgument", ...
             "Need a local path for file download as second argument.");
      try
        ids = {id};
        local_paths = {local_path};
        [entities, count_results] = ...
          maox_run_transaction(obj.connection, {}, {}, struct("", {}), struct("", {}), {}, ...
                               ids, local_paths);
        assert(count_results == -1, "maox:RuntimeError", "There should be no count results.");
        entity = entities{1};
      catch
        rethrow(lasterror());
      end

    end

  end
end
