% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%% Convert a struct array as received from maoxdb to a cell array of Entity objects.
function entities = maox_convert_collection(collection)
  entities = cell();
  if isempty(collection)       % We don't care about the type in this case.
    return
  end
  assert(isstruct(collection), "caosdb:InvalidArgument", ...
         ["1xN struct array required.\n " disp(collection)]);
  for data = collection
    ent = Entity(data);
    entities{end + 1} = ent;
    if ent.has_errors()
      warning("caosdb:transactionError", ["Entity `" ent.id "` has error(s):"]);
      for msg = ent.get_errors()
        disp(msg{1});
      end
    end
    if ent.has_warnings()
      warning("caosdb:transactionWarning", ["Entity `" ent.id "` has warning(s):"]);
      for msg = ent.get_warnings()
        disp(msg{1});
      end
    end
    % if ent.has_infos()
    %   warning("caosdb:transactionInfo", ["Entity `" ent.id "` has info(s):"]);
    %   for msg = ent.get_infos()
    %     disp(msg{1})
    %   end
    % end
  end
end
