% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%% The main function which intitializes the tests.
function test_suite = test_query_retrieve()
  try % assignment of 'localfunctions' is necessary in Matlab >= 2016
    test_functions = localfunctions();
  catch % no problem; early Matlab versions can use initTestSuite fine
  end
  initTestSuite;
end

%% Test retrieval with errors
function test_retrieve_failure()
  % Default connection configuration is sufficient.
  c = Caosdb();

  % Call retrieve with wrong arguments.
  % (Only m file level, no invalid arguments should make it to the mex function.)
  assertExceptionThrown(@()c.retrieve_by_id(120), "maox:InvalidArgument");
  assertExceptionThrown(@()c.retrieve_by_id({120}), "maox:InvalidArgument");
  assertExceptionThrown(@()c.retrieve_by_id({"120"}, {}), "maox:InvalidArgument");
  assertExceptionThrown(@()c.retrieve_by_id({"120"}, {120}), "maox:InvalidArgument");
  assertExceptionThrown(@()c.retrieve_by_id({"120"}, {"120", 120}), "maox:InvalidArgument");
end

%% Test query with errors
function test_query_failure()
  % Default connection configuration is sufficient.
  c = Caosdb();

  % Call retrieve with wrong arguments.
  % (Only m file level, no invalid arguments should make it to the mex function.)
  assertExceptionThrown(@()c.query(120), "maox:InvalidArgument");
  assertExceptionThrown(@()c.query({120}), "maox:InvalidArgument");
  assertExceptionThrown(@()c.query("FIND Record", "FIND Record"), "maox:InvalidArgument");
end
