=============
 Development
=============

Structure
=========

The sources for functions and classes are in ``src/``.  Private functions (mostly C++ source files
which are to be compiled into ``*.mex``) are implemented in
``private/maox_some_function.*``. (``maox`` stands for "mex file for CaosDB".

Writing Documentation
=====================

- The first string after the copyright block is used for documentation.  This implies that the
  copyright block must be recognized as such: A copyright line should be the first line there.
- Documentation should be written as TexInfo (see below for details) in order to be available in the
  online documentation.
- Extract documentation from file:
  ``[txt, form] = get_help_text_from_file(make_absolute_filename('pkg/inst/some_function.m'))``
- Generate HTML documentation from single file:

    .. code-block:: octave

       pkg uninstall caosdb
       pkg install caosdb.tar.gz
       pkg load caosdb
       html_help_text('some_function', './htdocs/some.html', 'octave-forge', pkgname="caosdb")
- Generate HTML documentation for a package:

    .. code-block:: octave

       generate_package_html('caosdb', 'htdocs', 'octave-forge')

Requirements
------------

- For tests, we use MOxUnit, which can be installed like this:

    .. code-block:: console

        wget --output-document MOxUnit-master.zip https://github.com/MOxUnit/MOxUnit/archive/master.zip
        unzip MOxUnit-master.zip
        cd MOxUnit-master
        make install
        cd ..

- We use a dedicated Octave package for documentation generation: generate_html_.  This package
  can be installed as follows:

    .. code-block:: octave

        pkg install "https://downloads.sourceforge.net/project/octave/Octave%20Forge%20Packages/Individual%20Package%20Releases/generate_html-0.3.2.tar.gz"

.. _generate_html: https://gnu-octave.github.io/packages/generate_html

TexInfo details
---------------

Here is an Example for texinfo documentation:..
  https://github.com/gnu-octave/octave/blob/default/scripts/geometry/inpolygon.m

Useful TexInfo commands:

- ``@deftypefn`` ::
    Define a function that can be called.  Example code:..
    ``@deftypefn{Function File} {@var{out} =} caosdb_exec (@var{arg})``

Debugging
=========

Debugging the C++ parts
-----------------------

For debugging, valgrind, gdb or other debugging tools may be used.  For example when segfaults occur
while calling Octave (or quitting Octave), they may be investigated like this:

``valgrind --trace-children=yes octave test/segfault_dummy.m``

The script file ``test/segfault_dummy.m`` reproduces the segfault in this example.  The output
indicates code which tries to perform illegal memory access.

Useful options for valgrind include:

``--exit-on-first-error=yes --error-exitcode=200``
   Fail-fast if there are errors.

