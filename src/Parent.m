% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%%
%
%  Note:  Setting the description has no effect for inserting or updating parents.
classdef Parent < handle

  properties
    id
    name
    description
  end

  methods

    % Constructor.
    % The structure of the DATA parameter follows the convention outline in `maoxdb.hpp`.
    function obj = Parent(data)
      narginchk(0, 1);
      if nargin == 1
        obj.id = data.id;
        obj.name = data.name;
        obj.description = data.description;
      end
    end

    % Convert to a struct which has all the fields that may be needed for interaction with the
    % maoxdb library.
    function struct_array = to_struct(obj)
      res = struct();
      res.id = obj.id;
      res.name = obj.name;
      res.description = obj.description;

      struct_array = res;
    end

    function disp(obj)
      disp(obj.to_struct());
    end

  end
end
